//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:37 CDT 2005
 *
 */
package org.nrg.xdat.om;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import org.nrg.xdat.om.base.BaseFsAutomaticsegmentationdata;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;

/**
 * @author XDAT
 *
 */
public class FsAutomaticsegmentationdata extends BaseFsAutomaticsegmentationdata {

	public FsAutomaticsegmentationdata(ItemI item)
	{
		super(item);
	}

	public FsAutomaticsegmentationdata(UserI user)
	{
		super(user);
	}

	public FsAutomaticsegmentationdata()
	{}

	public FsAutomaticsegmentationdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

	public String getCerebralCortex()
	{
		return getSummedValues("Cerebral-Cortex");
	}
	
	public String getCerebralWhiteMatter()
	{
		return getSummedValues("Cerebral-White-Matter");
	}
	
	public String getHippocampus()
	{
		return getSummedValues("Hippocampus");
	}
	
	public String getSummedValues(String region)
	{
		Integer l = getRegionVoxelCount(region,"left");
		Integer r = getRegionVoxelCount(region,"right");
		
		return (l.intValue() + r.intValue()) + "mm&#179; (left:" + l + "mm&#179;, right:" + r + "mm&#179;)";
	}
	
	public Integer getRegionVoxelCount(String regionName, String hemisphere)
	{
		Integer _return = new Integer(0);
	    try {
	        ArrayList al = (ArrayList)this.getRegions_region();
            Iterator regions = al.iterator();
            while (regions.hasNext())
            {
                XnatVolumetricregion region = (XnatVolumetricregion)regions.next();
                if (region.getName().equalsIgnoreCase(regionName))
                {
                    if (hemisphere == null || hemisphere.equals(""))
                    {
                        _return = region.getVoxels();
                        break;
                    }else{
                        if (region.getHemisphere().equalsIgnoreCase(hemisphere))
                        {
                            _return = region.getVoxels();
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("",e);
        }
		
		return _return;
	}
	
	public ArrayList getAdditionalRegions()
	{
		ArrayList al = new ArrayList();
		try {
            Iterator regions = this.getRegions_region().iterator();
            while (regions.hasNext())
            {
                XnatVolumetricregion region = (XnatVolumetricregion)regions.next();
            	String regionName  = region.getName();
  
            	if ((!regionName.equalsIgnoreCase("Hippocampus"))&& (!regionName.equalsIgnoreCase("Cerebral-Cortex"))&& (!regionName.equalsIgnoreCase("Cerebral-White-Matter")))
            	{
            		if (region.getVoxels().intValue() > 0)
            		{
            			ArrayList sub = new ArrayList();
            			sub.add(regionName);
            			sub.add(region.getVoxels());
            			al.add(sub);
            		}
            	}
            }
        } catch (Exception e) {
            logger.error("",e);
        }
		al.trimToSize();
		return al;
	}

}
