// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Dec 06 09:45:35 CST 2006
 *
 */
package org.nrg.xdat.om;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Hashtable;

import org.nrg.xdat.om.base.BaseCndaPettimecoursedata;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;

/**
 * @author XDAT
 *
 */
public class CndaPettimecoursedata extends BaseCndaPettimecoursedata {

	public CndaPettimecoursedata(ItemI item)
	{
		super(item);
	}

	public CndaPettimecoursedata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaPettimecoursedata(UserI user)
	 **/
	public CndaPettimecoursedata()
	{}

	public CndaPettimecoursedata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}


    public ArrayList getActivityByTime()
    {
        ArrayList all = new ArrayList();
        CndaPettimecoursedata timecourse = new CndaPettimecoursedata(getCurrentDBVersion(true));
        if (timecourse.getRegions_region().size()>0)
        {
            
            int rows = ((CndaPettimecoursedataRegion)timecourse.getRegions_region().get(0)).getTimeseries_activity().size();
            for (int index =0;index<rows;index++)
            {
                ArrayList row = new ArrayList();
                for(int i=0;i<timecourse.getRegions_region().size();i++)
                {
                    CndaPettimecoursedataRegion region = (CndaPettimecoursedataRegion)timecourse.getRegions_region().get(i);

                    CndaPettimecoursedataRegionActivity activity = (CndaPettimecoursedataRegionActivity)region.getTimeseries_activity().get(index);
                    
                    if (i==0)
                    {
                        NumberFormat formatter = NumberFormat.getInstance();
                        formatter.setGroupingUsed(false);
                        formatter.setMaximumFractionDigits(5);
                        formatter.setMinimumFractionDigits(5);
                        row.add(formatter.format(activity.getTime()));
                    }
                    
                    NumberFormat formatter = NumberFormat.getInstance();
                    formatter.setGroupingUsed(false);
                    formatter.setMaximumFractionDigits(4);
                    formatter.setMinimumFractionDigits(4);
                    row.add(formatter.format(activity.getActivity()));
                }
                all.add(row);
            }
        }
        
        return all;
    }
    
    public boolean existsTimeSeries(String regionName, String regionHemisphere) {
        boolean exists = false ;
        ArrayList regions = (ArrayList)getRegions_region();
        for (int i = 0 ; i < regions.size(); i++) {
            CndaPettimecoursedataRegion region = ((CndaPettimecoursedataRegion)regions.get(i));
            if (region.getName().equals(regionName) && region.getHemisphere().equals(regionHemisphere)) {
                exists = true;
                break;
            }
        }
        return exists;
    }
    
    public ArrayList getLoganPlots() {
        ArrayList rtn = new ArrayList();
        ArrayList outFile = (ArrayList)this.getOut_file();
        for (int i = 0; i < outFile.size(); i++) {
            XnatAbstractresource absRsc = (XnatAbstractresource)outFile.get(i);
            if (absRsc instanceof XnatResource) {
                XnatResource resource = (XnatResource)outFile.get(i);
                if (resource.getContent().equals("LOGANPLOT")) {
                    rtn.add(resource);
                }
            }
        }
        return rtn;
    }
    
    public XnatResource getPlot() {
        XnatResource rsc = new XnatResource();
        CndaPettimecoursedata timecourse = new CndaPettimecoursedata(getCurrentDBVersion(true));
        ArrayList outFile = (ArrayList)timecourse.getOut_file();
        for (int i = 0; i < outFile.size(); i++) {
            XnatAbstractresource absRsc = (XnatAbstractresource)outFile.get(i);
            if (absRsc instanceof XnatResource) {
                XnatResource resource = (XnatResource)outFile.get(i);
                if (resource.getContent().equals("PLOT")) {
                    rsc = resource;
                    break;
                }
            }
        }
        return rsc;
    }
    
    
}
