// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Mon Apr 13 09:49:53 CDT 2009
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
public interface PipePipelinedetailsI {

	public String getSchemaElementName();

	/**
	 * @return Returns the path.
	 */
	public String getPath();

	/**
	 * Sets the value for path.
	 * @param v Value to Set.
	 */
	public void setPath(String v);

	/**
	 * @return Returns the description.
	 */
	public String getDescription();

	/**
	 * Sets the value for description.
	 * @param v Value to Set.
	 */
	public void setDescription(String v);

	/**
	 * generatesElements/element
	 * @return Returns an ArrayList of org.nrg.xdat.om.PipePipelinedetailsElementI
	 */
	public ArrayList<org.nrg.xdat.om.PipePipelinedetailsElement> getGenerateselements_element();

	/**
	 * Sets the value for generatesElements/element.
	 * @param v Value to Set.
	 */
	public void setGenerateselements_element(ItemI v) throws Exception;

	/**
	 * @return Returns the customwebpage.
	 */
	public String getCustomwebpage();

	/**
	 * Sets the value for customwebpage.
	 * @param v Value to Set.
	 */
	public void setCustomwebpage(String v);

	/**
	 * parameters/parameter
	 * @return Returns an ArrayList of org.nrg.xdat.om.PipePipelinedetailsParameterI
	 */
	public ArrayList<org.nrg.xdat.om.PipePipelinedetailsParameter> getParameters_parameter();

	/**
	 * Sets the value for parameters/parameter.
	 * @param v Value to Set.
	 */
	public void setParameters_parameter(ItemI v) throws Exception;

	/**
	 * @return Returns the appliesTo.
	 */
	public String getAppliesto();

	/**
	 * Sets the value for appliesTo.
	 * @param v Value to Set.
	 */
	public void setAppliesto(String v);
}
