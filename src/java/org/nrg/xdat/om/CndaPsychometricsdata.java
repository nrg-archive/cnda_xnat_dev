//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:39 CDT 2005
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaPsychometricsdata extends BaseCndaPsychometricsdata {
    private XnatSubjectdata subject = null;
	public CndaPsychometricsdata(ItemI item)
	{
		super(item);
	}

	public CndaPsychometricsdata(UserI user)
	{
		super(user);
	}

	public CndaPsychometricsdata()
	{}

	public CndaPsychometricsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

	public XnatSubjectdata getSubjectData()
	{
	    if (subject==null)
	    {
	        ArrayList al = XnatSubjectdata.getXnatSubjectdatasByField("xnat:subjectData/ID",this.getSubjectId(),this.getUser(),false);
	        if (al.size()>0)
	        {
	            subject = (XnatSubjectdata)al.get(0);
	        }
	    }
	    return subject;
	}

}
