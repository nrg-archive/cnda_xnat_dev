// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Mon Apr 13 09:49:53 CDT 2009
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
public interface PipePipelinerepositoryI {

	public String getSchemaElementName();

	/**
	 * pipeline
	 * @return Returns an ArrayList of org.nrg.xdat.om.PipePipelinedetailsI
	 */
	public ArrayList<org.nrg.xdat.om.PipePipelinedetails> getPipeline();

	/**
	 * Sets the value for pipeline.
	 * @param v Value to Set.
	 */
	public void setPipeline(ItemI v) throws Exception;

	/**
	 * @return Returns the pipe_PipelineRepository_id.
	 */
	public Integer getPipePipelinerepositoryId();

	/**
	 * Sets the value for pipe_PipelineRepository_id.
	 * @param v Value to Set.
	 */
	public void setPipePipelinerepositoryId(Integer v);
}
