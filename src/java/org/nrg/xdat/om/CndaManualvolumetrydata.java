//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:38 CDT 2005
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.search.TableSearch;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaManualvolumetrydata extends BaseCndaManualvolumetrydata {
    ArrayList allCodes = null;
	public CndaManualvolumetrydata(ItemI item)
	{
		super(item);
	}

	public CndaManualvolumetrydata(UserI user)
	{
		super(user);
	}

	public CndaManualvolumetrydata()
	{}

	public CndaManualvolumetrydata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

	public ArrayList getVolumetryRegions()
	{
	    try {
            return (ArrayList)this.getRegions_region();
        } catch (Exception e) {
            logger.error("",e);
            return new ArrayList();
        }
	}
	
	public ArrayList getAllCodes()
	{
	    if (allCodes == null)
	    {
	        allCodes = new ArrayList();
		    String query = "SELECT DISTINCT name,hemisphere FROM xnat_volumetricregion vr JOIN cnda_manualvolumetryregion mvr ON vr.xnat_volumetricregion_id=mvr.xnat_volumetricregion_id";
		    try {
	            XFTTable table =  TableSearch.Execute(query,((XFTItem)this.getItem()).getGenericSchemaElement().getDbName(),"system");
	            
	            ArrayList al = new ArrayList();
	            al.add("name");
	            al.add("hemisphere");
	            al.trimToSize();
	            
	            ArrayList values = table.convertColumnsToArrayList(al);
	            allCodes = values;
	        } catch (ElementNotFoundException e) {
	            logger.error("",e);
	        } catch (Exception e) {
	            logger.error("",e);
	        }
	    }
	    return allCodes;
	}
	
	public CndaManualvolumetryregion getVolumetryRegionByName(String name)
	{
	    for (int i=0;i<this.getVolumetryRegions().size();i++)
		{
			if (((CndaManualvolumetryregion)getVolumetryRegions().get(i)).getName().equalsIgnoreCase(name))
			{
				return (CndaManualvolumetryregion)getVolumetryRegions().get(i);
			}
		}
	    return null;
	}
	
	public CndaManualvolumetryregion getVolumetryRegionByName(String name, String hemisphere)
	{
	    for (int i=0;i<this.getVolumetryRegions().size();i++)
		{
			if (((CndaManualvolumetryregion)getVolumetryRegions().get(i)).getName().equalsIgnoreCase(name)
			        &&(hemisphere.equals("") || (hemisphere.equalsIgnoreCase(((CndaManualvolumetryregion)getVolumetryRegions().get(i)).getHemisphere()))))
			{
				return (CndaManualvolumetryregion)getVolumetryRegions().get(i);
			}
		}
	    return null;
	}

}
