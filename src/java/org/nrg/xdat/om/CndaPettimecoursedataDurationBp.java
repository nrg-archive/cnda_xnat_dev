// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Dec 07 13:36:39 CST 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.text.NumberFormat;
import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaPettimecoursedataDurationBp extends BaseCndaPettimecoursedataDurationBp {

	public CndaPettimecoursedataDurationBp(ItemI item)
	{
		super(item);
	}

	public CndaPettimecoursedataDurationBp(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaPettimecoursedataDurationBp(UserI user)
	 **/
	public CndaPettimecoursedataDurationBp()
	{}

	public CndaPettimecoursedataDurationBp(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

    public String getSlopeText()
    {
        Double slope = getBp() + 1;
        NumberFormat formatter = NumberFormat.getInstance();
        formatter.setGroupingUsed(false);
        formatter.setMaximumFractionDigits(6);
        formatter.setMinimumFractionDigits(6);
        return formatter.format(slope);
    }

    public String getBpText()
    {
        NumberFormat formatter = NumberFormat.getInstance();
        formatter.setGroupingUsed(false);
        formatter.setMaximumFractionDigits(6);
        formatter.setMinimumFractionDigits(6);
        return formatter.format(getBp());
    }

    public String getCorrelText()
    {
        NumberFormat formatter = NumberFormat.getInstance();
        formatter.setGroupingUsed(false);
        formatter.setMaximumFractionDigits(6);
        formatter.setMinimumFractionDigits(6);
        return formatter.format(getCorrel());
    }

    public String getSlopeText(int precision)
    {
        Double slope = getBp() + 1;
        NumberFormat formatter = NumberFormat.getInstance();
        formatter.setGroupingUsed(false);
        formatter.setMaximumFractionDigits(precision);
        formatter.setMinimumFractionDigits(precision);
        return formatter.format(slope);
    }

    public String getBpText(int precision)
    {
        NumberFormat formatter = NumberFormat.getInstance();
        formatter.setGroupingUsed(false);
        formatter.setMaximumFractionDigits(precision);
        formatter.setMinimumFractionDigits(precision);
        return formatter.format(getBp());
    }

    public String getCorrelText(int precision)
    {
        NumberFormat formatter = NumberFormat.getInstance();
        formatter.setGroupingUsed(false);
        formatter.setMaximumFractionDigits(precision);
        formatter.setMinimumFractionDigits(precision);
        return formatter.format(getCorrel());
    }

}
