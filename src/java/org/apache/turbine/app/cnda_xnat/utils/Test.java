//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Created on Aug 19, 2005
 *
 */
package org.apache.turbine.app.cnda_xnat.utils;

import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.presentation.HTMLPresenter;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.XFTTableI;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xft.utils.XMLUtils;

/**
 * @author Tim
 *
 */
public class Test {
	static org.apache.log4j.Logger logger = Logger.getLogger(Test.class);

    public static void main(String[] args) {
        try {
			//String appDir = "C:\\xdat\\projects\\sample";
			String appDir = "C:\\xdat\\projects\\cnda_xnat\\";
			
			//XDATTool tool = new XDATTool(appDir,"admin","");
			XDAT.init(appDir,false,true);
			
			
			XDATUser user = new XDATUser("admin","admin");
			
//			XFTItem item = ItemSearch.GetItem("cnda:atlasScalingFactorData/ID","990413_97427_ASF_9501161738",user,false);
//			CndaAtlasscalingfactordata asf = new CndaAtlasscalingfactordata(item);
//			XnatMrsessiondata mr = asf.getMrSessionData();
			
//			XFTItem item = ItemSearch.GetItem("fs:automaticSegmentationData/ID","000223_95761_ASEG_9503271337",null,false);
//			Object o = XMLWriter.ItemToDOM(item,true,"");
//			XFTItem item = ItemSearch.GetItem("xnat:mrSessionData/ID","990426_92577",null,false);
//			XnatMrsessiondata mr = new XnatMrsessiondata(item);
//			mr.getMinimalLoadAssessors();
			//ArrayList al = mr.getExtraFiles();
			//XnatMrsessiondata mr = XnatMrsessiondata.getXnatMrsessiondatasByField("xnat:mrSessionData.ID","")
//			XFTItem item = ItemSearch.GetItem("xdat:security/system","CNDA",null,false);
//			XMLUtils.DOMToFile(item.toXML(),appDir + "work\\system.xml");

			//Hashtable hash =XNATUtils.getInvestigatorsForCreate("xnat:subjectData",user);
			
			XnatMrsessiondata mr = new XnatMrsessiondata(ItemSearch.GetItem("xnat:mrSessiondata.ID","000107_vc1543",user,false));

			XMLUtils.DOMToFile(mr.toJoinedXML(),appDir + "\\work\\000107_vc1543_joined.xml");
			
			//org.nrg.xft.XFTTool.StoreXMLFileToDB(appDir + "\\work\\dm1.xml",user,Boolean.FALSE,true);
			
//			XFTItem item = XFTItem.NewItem("xnat:subjectData",user);
//			XFTItem demo = XFTItem.NewItem("xnat:demographicData",user);
//			XFTItem me = XFTItem.NewItem("xnat:subjectMetadata",user);
//			item.setProperty("demographics",demo);
//			item.setProperty("metadata",me);
//			
//			item.setProperty("ID","XNAT001");
//			item.setProperty("investigator.xnat_investigatorData_id",new Integer(1));
//			item.setProperty("demographics.gender","female");
			
//			item.setProperty("xnat:subjectdata.demographics.gender","female");
//			item.setProperty("xnat:subjectdata/addid[0]","123456");
//			item.setProperty("xnat:subjectdata/addid[0]/name","map");
//			item.setProperty("xnat:subjectdata/demographics/dob.date","2");
//			item.setProperty("xnat:subjectdata/demographics/dob.month","2");
//			item.setProperty("xnat:subjectdata/demographics/dob.year","1903");
//			item.setProperty("xnat:subjectdata/demographics/education","12");
//			item.setProperty("xnat:subjectdata/demographics/handedness","left");
//			item.setProperty("xnat:subjectdata/demographics/ses","1");
//			item.setProperty("xnat:subjectdata/id","XNAT001");
//			item.setProperty("xnat:subjectdata/investigator_xnat_investigatordata_id","1");
//			item.setProperty("xnat:subjectdata/metadata/cohort","cohort");
			
//			item.save(user,true);
			
			DisplaySearch ds = user.getSearch("cnda:segmentationFastData","listing");
			XFTTableI t = ds.execute(new HTMLPresenter(""),"admin");
			
			
//			Document doc = XFTTool.FindXML("xnat:Project.ID","CNDA0",null);
//			XMLUtils.DOMToFile(doc,appDir + "\\work\\CNDA.xml");
			System.out.println("");
		}catch(ElementNotFoundException e)
		{
			e.printStackTrace();
		}catch(XFTInitException e)
		{
			e.printStackTrace();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
			
    }
}
