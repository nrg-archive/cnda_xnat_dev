//Copyright 2007 Washington University School of Medicine All Rights Reserved
/*
 * Created on Jan 16, 2008
 *
 */
package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureReport;

public class XDATScreen_validate extends SecureReport {

    @Override
    public void finalProcessing(RunData data, Context context) {
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ("MM/dd/yyyy");
        context.put("currentDate", formatter.format(Calendar.getInstance().getTime()));
    }

}
