/*
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 *
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.launchers.GenericDTIProcessingLauncher;
import org.nrg.xdat.om.ArcPipelineparameterdata;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;

@SuppressWarnings("unused")
public class PipelineScreen_GenericDTIProcessing extends DefaultPipelineScreen{
	String mprageScanType;
	String tseScanType;
	String dwiScanType;

	static Logger logger = Logger.getLogger(PipelineScreen_GenericDTIProcessing.class);


    public void finalProcessing(RunData data, Context context){
        try {
            XnatMrsessiondata mr = (XnatMrsessiondata) om;
            context.put("mr", mr);

            ArcPipelineparameterdata mprageParam = getProjectPipelineSetting(GenericDTIProcessingLauncher.MPRAGE_PARAM);
            if (mprageParam != null) {
               mprageScanType = mprageParam.getCsvvalues();
            }
            ArcPipelineparameterdata tseParam = getProjectPipelineSetting(GenericDTIProcessingLauncher.TSE_PARAM);
            if (tseParam != null) {
                tseScanType = tseParam.getCsvvalues();
            }

            ArcPipelineparameterdata dwiParam = getProjectPipelineSetting(GenericDTIProcessingLauncher.DWI_PARAM);
            if (dwiParam != null) {
                dwiScanType = dwiParam.getCsvvalues();
            }

            String selfStatus = WrkWorkflowdata.GetLatestWorkFlowStatusByPipeline(mr.getId(), XnatMrsessiondata.SCHEMA_ELEMENT_NAME, GenericDTIProcessingLauncher.LOCATION+File.separator +GenericDTIProcessingLauncher.NAME, mr.getProject(), TurbineUtils.getUser(data));
            if (selfStatus.equalsIgnoreCase(WrkWorkflowdata.COMPLETE)) {
                data.setMessage("This pipeline has already completed. Relaunching the pipeline may result in loss of processed files");
            }
            String pathToTarget = getTarget();
            context.put("target",pathToTarget);
            context.put("target_name",getTargetName(pathToTarget));
            context.put("projectSettings", projectParameters);
            LinkedHashMap<String,String> buildableScanTypes = new LinkedHashMap<String,String>();
            if (mprageScanType != null)
                buildableScanTypes.put(GenericDTIProcessingLauncher.MPRAGE, mprageScanType);
            if (tseScanType != null)
                buildableScanTypes.put(GenericDTIProcessingLauncher.T2W,tseScanType);
            if (dwiScanType != null)
                buildableScanTypes.put(GenericDTIProcessingLauncher.DWI,dwiScanType);
            context.put("buildableScanTypes", buildableScanTypes);

        } catch(Exception e) {
            logger.error("Possibly the project wide pipeline has not been set", e);
            e.printStackTrace();
        }
    }

    public void preProcessing(RunData data, Context context)   {
	     super.preProcessing(data, context);
  	 }

	protected String getTarget() {
		String rtn = null;
		for (Map.Entry<String,ArcPipelineparameterdata> paramsEntry : projectParameters.entrySet()) {
			if (paramsEntry.getKey().equalsIgnoreCase("target")) {
				rtn = paramsEntry.getValue().getCsvvalues();
				break;
			}
		}
		return rtn;
	}

	protected String getTargetName(String atlasPath) {
		String rtn = null; if (atlasPath == null) return rtn;
		rtn = atlasPath;
		int indexOfLastSlash = atlasPath.lastIndexOf(File.separator);
		if (indexOfLastSlash != -1) {
			String targetName = atlasPath.substring(indexOfLastSlash + 1);
			int indexOfDot = targetName.indexOf(".");
			if (indexOfDot != -1) {
			   rtn = targetName.substring(0,indexOfDot);
			}
			else {
				rtn = targetName;
			}
		}
		return rtn;
	}
}
