/*
 * GENERATED FILE
 * Created on Mon Aug 09 15:50:02 CDT 2010
 *
 */
package org.apache.turbine.app.cnda_xnat.modules.screens;
import java.text.DecimalFormat;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.GhfFdgcardioassessor;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.apache.commons.math.stat.descriptive.*;

/**
 * @author XDAT
 *
 */
public class XDATScreen_report_ghf_fdgCardioAssessor extends SecureReport {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XDATScreen_report_ghf_fdgCardioAssessor.class);
	/* (non-Javadoc)
	 * @see org.nrg.xdat.turbine.modules.screens.SecureReport#finalProcessing(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
	 */
	private static final DecimalFormat threePlaces = new DecimalFormat("0.000");
	
	public void finalProcessing(RunData data, Context context) {
		GhfFdgcardioassessor gfca = (GhfFdgcardioassessor) om;
		
		// calculate RPP for each time point
		Double baselineheart_rpp = gfca.getBaselineheart_sbp() * gfca.getBaselineheart_hr();
		Double thirtyminheart_rpp = gfca.getThirtyminheart_sbp() * gfca.getThirtyminheart_hr();
		Double sixtyminheart_rpp = gfca.getSixtyminheart_sbp() * gfca.getSixtyminheart_hr();
		
		context.put("baselinerpp", baselineheart_rpp.toString());
		context.put("thirtyminrpp", thirtyminheart_rpp);
		context.put("sixtyminrpp", sixtyminheart_rpp);
		
		// calculate mean/stddev/cov for glucose
		calculateStats("gluc", context,
				   gfca.getFiveminplasma_gluc(),
				   gfca.getThirtyminplasma_gluc(),
				   gfca.getSixtyminplasma_gluc());
		
		// calculate mean/stddev/cov for FFA
		calculateStats("ffa", context,
				   gfca.getFiveminplasma_ffa(),
				   gfca.getThirtyminplasma_ffa(),
				   gfca.getSixtyminplasma_ffa());
		
		// calculate sbp mean/stddev/cov
		calculateStats("sbp", context,
					   gfca.getBaselineheart_sbp().doubleValue(),
					   gfca.getThirtyminheart_sbp().doubleValue(),
					   gfca.getSixtyminheart_sbp().doubleValue());
		
		// calculate dbp mean/stddev/cov
		calculateStats("dbp", context,
				   gfca.getBaselineheart_dbp().doubleValue(),
				   gfca.getThirtyminheart_dbp().doubleValue(),
				   gfca.getSixtyminheart_dbp().doubleValue());
		
		// calculate hr mean/stddev/cov
		calculateStats("hr", context,
				   gfca.getBaselineheart_hr().doubleValue(),
				   gfca.getThirtyminheart_hr().doubleValue(),
				   gfca.getSixtyminheart_hr().doubleValue());
		
		// calculate rpp mean/stddev/cov
		calculateStats("rpp", context,
				   baselineheart_rpp.doubleValue(),
				   thirtyminheart_rpp.doubleValue(),
				   sixtyminheart_rpp.doubleValue());
		
		// calculate MGU, Ki * average glucose
		// grab the value calculateStats put in the context rather than recalculating it
		Double glucMean = Double.parseDouble((String)context.get("glucmean"));
		
		Double mgu = gfca.getKi_roi1() * glucMean; // only ROI1 because we only have one value for Ki now
		
		context.put("mgu", threePlaces.format(mgu));
	}
	
	private void calculateStats(String prefix, Context context, double... values) {
		SummaryStatistics sumstats = new SummaryStatistics();
		
		for (Double val : values)
			sumstats.addValue(val);
		
		context.put(prefix + "mean", threePlaces.format(sumstats.getMean()));
		context.put(prefix + "stddev", threePlaces.format(sumstats.getStandardDeviation()));
		context.put(prefix + "cov", threePlaces.format((sumstats.getStandardDeviation() / sumstats.getMean()) * 100));
	}	
}
