//Copyright Washington University School of Medicine All Rights Reserved
/*
 * Created on Feb 15, 2007
 *
 */
package org.apache.turbine.app.cnda_xnat.modules.actions;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xdat.turbine.modules.actions.BundleAction;
import org.nrg.xdat.turbine.modules.actions.DisplaySearchAction;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.TurbineUtils;

public class PlotImageAction extends SecureAction {
    public void doPerform(RunData data, Context context) throws Exception
    {        
        if (data.getParameters().get("querytype") !=null)
        {
            if(data.getParameters().getString("querytype").equals("new"))
            {
                DisplaySearchAction dsa = new DisplaySearchAction();
                DisplaySearch ds = dsa.setupSearch(data,context);
                TurbineUtils.setSearch(data,ds);
                data.setScreenTemplate("PlotImage.vm");
                return;
            }
        }
        
        if (data.getParameters().get("bundle") !=null)
        {
            String bundle = data.getParameters().get("bundle");
            BundleAction ba = new BundleAction();
            DisplaySearch ds = ba.setupSearch(data, context);
            TurbineUtils.setSearch(data,ds);
            data.setScreenTemplate("PlotImage.vm");
            return;
        }
        

        data.setScreenTemplate("PlotImage.vm");
        return;
    }
}