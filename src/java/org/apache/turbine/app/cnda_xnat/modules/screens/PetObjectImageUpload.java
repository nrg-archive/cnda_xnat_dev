/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureReport;

public class PetObjectImageUpload extends SecureReport{

    public void finalProcessing(RunData data, Context context)
    {
        context.put("pet", data.getSession().getAttribute("pet_image_upload"));
        //data.getSession().removeAttribute("pet_image_upload");
        context.put("miscFiles",data.getSession().getAttribute("miscFiles"));
        context.put("step",2);
        context.put("step1Disabled","");
        context.put("step2Disabled","disabled");
        context.put("step3Disabled","");
    }
}
