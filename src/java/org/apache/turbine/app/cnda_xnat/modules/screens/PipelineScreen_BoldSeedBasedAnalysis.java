/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.PipelineRepositoryManager;
import org.nrg.pipeline.launchers.GenericBoldPreProcessingLauncher;
import org.nrg.xdat.om.ArcProject;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

public class PipelineScreen_BoldSeedBasedAnalysis extends DefaultPipelineScreen  {

	static Logger logger = Logger.getLogger(PipelineScreen_BoldSeedBasedAnalysis.class);
 	
	 public void finalProcessing(RunData data, Context context){
		 try {
			XnatMrsessiondata mr = (XnatMrsessiondata) om;
	    	context.put("mr", mr); 

	    	List boldRecons = mr.getReconstructionsByType("BOLD");
	    	
	    	if (boldRecons == null || boldRecons.size() ==0 ) {
	    		data.setMessage("The BOLD scans for this session haven't been processed yet. The BOLD scans must be processed before this analysis can be performed. Go <a href=\"XDATActionRouter/xdataction/PipelineScreen_launch_pipeline/search_element/xnat%3AmrSessionData/search_field/xnat%3AmrSessionData.ID/search_value/" + mr.getId() + "/popup/true/project/" + mr.getProject() + "\">here</a> to process the scans.");
	    		context.put("preprocessed", 0);
	    	}else {
	    		context.put("preprocessed", 1);
	    	}
	    	 ArcProject arcProject = ArcSpecManager.GetInstance().getProjectArc(mr.getProject());
	    	 String partialPipelinePath = GenericBoldPreProcessingLauncher.LOCATION + "/" + GenericBoldPreProcessingLauncher.NAME;
	    	 String pipePath = PipelineRepositoryManager.GetPathToPipeline(partialPipelinePath, arcProject);
	    	setParameters(pipePath);
	    	context.put("projectSettings", projectParameters);
		 }catch(Exception e) {
			 logger.error("Possibly the project wide pipeline has not been set", e);
			 e.printStackTrace();
		 }
	 }
	 
	 public void preProcessing(RunData data, Context context)   {
	     super.preProcessing(data, context);
  	 }

}
