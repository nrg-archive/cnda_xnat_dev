//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
package org.apache.turbine.app.cnda_xnat.modules.screens;



import org.apache.turbine.util.RunData;


import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;

/**
 * Grab all the records in a table using a Peer, and
 * place the Vector of data objects in the context
 * where they can be displayed by a #foreach loop.
 *
 * @author <a href="mailto:jvanzyl@periapt.com">Jason van Zyl</a>
 */
public class BuildPipelineVerify extends SecureReport
{

    public void preProcessing(RunData data, Context context)
    {
        TurbineUtils.InstanciatePassedItemForScreenUse(data,context);
    }
    
	
    /**
     * Place all the data object in the context
     * for use in the template.
     */
    public void finalProcessing(RunData data, Context context) 
    {
		if (context.get("om")==null) {
			data.setScreenTemplate("PermissionError.vm");
			return;	
		}
    }
 
    

}
