// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Apr 03 14:45:58 CDT 2007
 *
 */
package org.apache.turbine.app.cnda_xnat.modules.screens;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.CndaPettimecoursedata;
import org.nrg.xdat.om.CndaPettimecoursedataDuration;
import org.nrg.xdat.om.CndaPettimecoursedataDurationBp;
import org.nrg.xdat.turbine.modules.screens.SecureReport;

/**
 * @author XDAT
 *
 */
public class XDATScreen_report_cnda_petTimeCourseData extends SecureReport {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XDATScreen_report_cnda_petTimeCourseData.class);
	/* (non-Javadoc)
	 * @see org.nrg.xdat.turbine.modules.screens.SecureReport#finalProcessing(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
	 */
	public void finalProcessing(RunData data, Context context) {
        Hashtable d20_60 = new Hashtable();
        Hashtable d30_60 = new Hashtable();
        CndaPettimecoursedata timeCourse = (CndaPettimecoursedata)om;
            ArrayList durations = (ArrayList)timeCourse.getDurations_duration();
            if (durations != null && durations.size()>0) {
                for (int i = 0; i < durations.size(); i++) {
                    CndaPettimecoursedataDuration duration = (CndaPettimecoursedataDuration)durations.get(i);
                    if (duration.getSpan().equals("20-60 Minutes")) {
                        ArrayList bps = (ArrayList)duration.getBp();
                        for (int j = 0; j < bps.size(); j++ ) {
                            CndaPettimecoursedataDurationBp bp = (CndaPettimecoursedataDurationBp)bps.get(j);
                            d20_60.put(bp.getName(), bp);
                        }
                    }else if (duration.getSpan().equals("30-60 Minutes")) {
                        ArrayList bps = (ArrayList)duration.getBp();
                        for (int j = 0; j < bps.size(); j++ ) {
                            CndaPettimecoursedataDurationBp bp = (CndaPettimecoursedataDurationBp)bps.get(j);
                            d30_60.put(bp.getName(), bp);
                        }
                    }
                }
            }
        Vector sorted_durations = new Vector(d30_60.keySet());
        Collections.sort(sorted_durations);
        context.put("sorted_durations",sorted_durations);
        context.put("d20_60",d20_60);
        context.put("d30_60",d30_60);
	}

}
