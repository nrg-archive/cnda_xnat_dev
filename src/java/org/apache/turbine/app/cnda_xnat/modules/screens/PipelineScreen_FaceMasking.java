/*
* @author John Flavin flavinj@mir.wustl.edu
*/

package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.io.File;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;
import java.util.LinkedHashMap;

import org.nrg.xdat.model.ArcPipelinedataI;
import org.nrg.xdat.om.ArcProject;
import org.nrg.xdat.om.ArcProjectPipeline;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.ArcPipelineparameterdata;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatSubjectassessordata;
// import org.nrg.xdat.model.XnatSubjectassessordataI;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xft.ItemI;

public class PipelineScreen_FaceMasking extends DefaultPipelineScreen {

    public static final String MPRAGE = "MPRAGE";
    public static final String SCANTYPE_PARAM = "scanTypes";

    public static final String MRXSITYPE = "xnat:mrSessionData";
    public static final String PETXSITYPE = "xnat:petSessionData";

    String mprageScanType;
    static Logger logger = Logger.getLogger(PipelineScreen_FaceMasking.class);
    public void finalProcessing(RunData data, Context context){

        try {
            String projectId = ((String)TurbineUtils.GetPassedParameter("project",data));
            String pipelinePath = ((String)TurbineUtils.GetPassedParameter("pipeline",data));
            String schemaType = ((String)TurbineUtils.GetPassedParameter("schema_type",data));
            ArcProject arcProject = ArcSpecManager.GetFreshInstance().getProjectArc(projectId);

            if (schemaType.equals(XnatProjectdata.SCHEMA_ELEMENT_NAME)) {
                ArcProjectPipeline pipelineData = (ArcProjectPipeline)arcProject.getPipelineByPath(pipelinePath);
                context.put("pipeline", pipelineData);
                setParameters(pipelineData, context);
            }else {
                ArcPipelinedataI pipelineData = arcProject.getPipelineForDescendantByPath(schemaType, pipelinePath);
                context.put("pipeline", pipelineData);
                setParameters(pipelineData, context);
            }

            context.put("mpragekey", MPRAGE);

            XnatImagesessiondata imageSession = null;
            if (schemaType.equalsIgnoreCase(MRXSITYPE)) {
                imageSession = (XnatMrsessiondata) om;
            } else if (schemaType.equalsIgnoreCase(PETXSITYPE)) {
                imageSession = (XnatPetsessiondata) om;
            }
            context.put("imageSession", imageSession);

            ArcPipelineparameterdata mprageParam = getProjectPipelineSetting(SCANTYPE_PARAM);
            if (mprageParam != null) {
                mprageScanType = mprageParam.getCsvvalues();
                setHasDicomFiles(imageSession, mprageScanType, context);
            }
            String selfStatus = WrkWorkflowdata.GetLatestWorkFlowStatusByPipeline(imageSession.getId(), schemaType, pipelinePath, imageSession.getProject(), TurbineUtils.getUser(data));
            if (selfStatus.equalsIgnoreCase(WrkWorkflowdata.COMPLETE)) {
                data.setMessage("This pipeline has already completed. Relaunching the pipeline may result in loss of processed files");
            }

            context.put("projectSettings", projectParameters);

            LinkedHashMap<String,String> buildableScanTypes = new LinkedHashMap<String,String>();
            if (mprageScanType != null) {
                buildableScanTypes.put(MPRAGE, mprageScanType);
            }

            context.put("buildableScanTypes", buildableScanTypes);


            ArrayList<XnatImagesessiondata> allImageSessions = new ArrayList<XnatImagesessiondata>();
            LinkedHashMap<String,ArrayList<XnatImagescandata>> allScans = new LinkedHashMap<String,ArrayList<XnatImagescandata>>();
            ItemI subjectXFT = ItemSearch.GetItem("xnat:subjectData.ID",(String)imageSession.getSubjectId(),TurbineUtils.getUser(data),false);
            XnatSubjectdata subject = new XnatSubjectdata(subjectXFT);
            Iterator expts = subject.getExperiments_experiment().iterator();
            while (expts.hasNext()) {
                XnatSubjectassessordata expt = (XnatSubjectassessordata)expts.next();
                String xsi = expt.getXSIType();
                if (expt.getProject().equalsIgnoreCase(projectId)) {
                    ArrayList<XnatImagescandata> scans = null;
                    String exptId = "";
                    if (xsi.equalsIgnoreCase(MRXSITYPE)) {
                        XnatMrsessiondata mrExpt = (XnatMrsessiondata)expt;
                        scans = mrExpt.getUnionOfScansByType(mprageScanType);
                        exptId = mrExpt.getId();

                        if (scans != null && scans.size() > 0) {
                            allImageSessions.add(mrExpt);
                        }
                    } else if (xsi.equalsIgnoreCase(PETXSITYPE)) {
                        XnatPetsessiondata petExpt = (XnatPetsessiondata)expt;
                        scans = petExpt.getUnionOfScansByType(mprageScanType);
                        exptId = petExpt.getId();
                        if (scans != null && scans.size() > 0) {
                            allImageSessions.add(petExpt);
                        }
                    }

                    if (scans != null && scans.size() > 0) {
                        // Collections.sort(scans, new Comparator<XnatImagescandata>() {
                        //     public int compare(XnatImagescandata one, XnatImagescandata other) {
                        //         return one.getId().compareTo(other.getId());
                        //     }
                        // });
                        Collections.sort(scans,XnatImagescandata.GetComparator());
                        allScans.put(exptId,scans);
                    }
                }
            }

            Collections.sort(allImageSessions, new Comparator<XnatImagesessiondata>() {
                public int compare(XnatImagesessiondata one, XnatImagesessiondata other) {
                    return one.getLabel().compareTo(other.getLabel());
                }
            });
            context.put("sessions",allImageSessions);
            context.put("allScans",allScans);
        }catch(Exception e) {
            logger.error("Possibly the project wide pipeline has not been set", e);
            e.printStackTrace();
        }
    }
}

// ItemI user = TurbineUtils.getUser(data)
// ItemI subject = ItemSearch.GetItem("xnat:subjectData.ID",(String)om.getSubjectId(),user,false);