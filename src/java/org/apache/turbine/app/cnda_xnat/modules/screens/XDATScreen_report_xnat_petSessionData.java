// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Jan 04 11:55:03 CST 2007
 *
 */
package org.apache.turbine.app.cnda_xnat.modules.screens;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.PipelineManager;
import org.nrg.xdat.om.CndaPettimecoursedata;
import org.nrg.xdat.om.CndaPettimecoursedataDuration;
import org.nrg.xdat.om.CndaPettimecoursedataDurationBp;
import org.nrg.xdat.om.XnatComputationdata;
import org.nrg.xdat.om.XnatInvestigatordata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.om.XnatReconstructedimagedata;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;

/**
 * @author XDAT
 *
 */
public class XDATScreen_report_xnat_petSessionData extends SecureReport {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XDATScreen_report_xnat_petSessionData.class);
	/* (non-Javadoc)
	 * @see org.nrg.xdat.turbine.modules.screens.SecureReport#finalProcessing(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
	 */
    public void finalProcessing(RunData data, Context context) {
        try{
            
            org.nrg.xdat.om.XnatPetsessiondata om = new org.nrg.xdat.om.XnatPetsessiondata(item);
            context.put("om",om);
            
            if (om.getInvestigator()==null)
            {
                Object id = om.getInvestigatorFK();
                if (id!=null)
                {
                    XnatInvestigatordata invest = (XnatInvestigatordata)XnatInvestigatordata.getXnatInvestigatordatasByXnatInvestigatordataId(id, TurbineUtils.getUser(data), false);
                    if (invest!=null)
                    {
                        om.setInvestigator((ItemI)invest);
                    }
                }
            }
            
            //System.out.println("Loaded om object (org.nrg.xdat.om.XnatPetsessiondata) as context parameter 'om'.");
            context.put("subject",om.getSubjectData());
            context.put("frames", getFramesAsText(om));
            ArrayList recons = (ArrayList)om.getReconstructions_reconstructedimage();
            Hashtable translationError = new Hashtable();
            Hashtable rotationError = new Hashtable();
            Hashtable aboveThreshold = new Hashtable();
            if (recons != null && recons.size() > 0) {
                XnatReconstructedimagedata recon = (XnatReconstructedimagedata)recons.get(0);
                ArrayList translationErrors = recon.getComputationByName("Translation Error");
                ArrayList rotationErrors = recon.getComputationByName("Rotation Error");
                if (translationErrors != null && rotationErrors != null && translationErrors.size() > 0 && rotationErrors.size() > 0) {
                    for (int i = 0; i < translationErrors.size(); i++) {
                        boolean aboveThresh = false;
                        XnatComputationdata te = (XnatComputationdata)translationErrors.get(i);
                        translationError.put(te.getSource(),new Double(Double.parseDouble(te.getValue())));
                        if (Double.parseDouble(te.getValue()) > 1) {
                            aboveThresh = true;
                        }
                        aboveThreshold.put(te.getSource(),new Boolean(aboveThresh));
                        te = (XnatComputationdata)rotationErrors.get(i);
                        rotationError.put(te.getSource(),new Double(Double.parseDouble(te.getValue())));
                    }
                }
            }    
            context.put("translation", translationError);
            context.put("rotation", rotationError);
            context.put("aboveThreshold",aboveThreshold);
            ArrayList timeCourses = om.getAssessors("cnda:petTimeCourseData");
            Hashtable d20_60 = new Hashtable();
            Hashtable d30_60 = new Hashtable();
            if (timeCourses.size() > 0) {
                CndaPettimecoursedata timeCourse = (CndaPettimecoursedata)timeCourses.get(0);
                ArrayList durations = (ArrayList)timeCourse.getDurations_duration();
                if (durations != null && durations.size()>0) {
                    for (int i = 0; i < durations.size(); i++) {
                        CndaPettimecoursedataDuration duration = (CndaPettimecoursedataDuration)durations.get(i);
                        if (duration.getSpan().equals("20-60 Minutes")) {
                            ArrayList bps = (ArrayList)duration.getBp();
                            for (int j = 0; j < bps.size(); j++ ) {
                                CndaPettimecoursedataDurationBp bp = (CndaPettimecoursedataDurationBp)bps.get(j);
                                d20_60.put(bp.getName(), bp);
                            }
                        }else if (duration.getSpan().equals("30-60 Minutes")) {
                            ArrayList bps = (ArrayList)duration.getBp();
                            for (int j = 0; j < bps.size(); j++ ) {
                                CndaPettimecoursedataDurationBp bp = (CndaPettimecoursedataDurationBp)bps.get(j);
                                d30_60.put(bp.getName(), bp);
                            }
                        }
                    }
                }
            }
            Vector sorted_durations = new Vector(d30_60.keySet());
            Collections.sort(sorted_durations);
            context.put("sorted_durations",sorted_durations);
            context.put("d20_60",d20_60);
            context.put("d30_60",d30_60);
            ArrayList workflows = PipelineManager.getWorkFlowsOrderByLaunchTimeDesc(om.getId(), "xnat:petSessionData",TurbineUtils.getUser(data));
            context.put("workflows",workflows);
            //System.out.println("Loaded subject object (org.nrg.xdat.om.XnatSubjectdata) as context parameter 'subject'.");
        } catch(Exception e){
            logger.debug("PetSessionData report ", e);
        }
    }
    
    
    
    private String getFramesAsText(XnatPetsessiondata pet) {
        String rtn = "";
        ArrayList recons = (ArrayList)pet.getReconstructions_reconstructedimage();
        if (recons == null || recons.size() == 0) {
            return rtn;
        }    
        XnatReconstructedimagedata recon = (XnatReconstructedimagedata)recons.get(0);
        ArrayList sFrames = recon.getComputationByName("Start Frame");
        if (sFrames == null || sFrames.size() == 0) return rtn;
        XnatComputationdata startFrameDatum = (XnatComputationdata)sFrames.get(0);
        if (startFrameDatum != null) {
            ArrayList lFrames = recon.getComputationByName("Last Frame");
            if (lFrames == null || lFrames.size() == 0) return rtn;
            XnatComputationdata lastFrameDatum = (XnatComputationdata)lFrames.get(0);
            if (lastFrameDatum != null) {
                String[] startFrames = StringUtils.split(startFrameDatum.getValue());
                String[] lastFrames = StringUtils.split(lastFrameDatum.getValue());
                if (startFrames != null && lastFrames != null && startFrames.length==lastFrames.length) {
                    for (int i = 0; i < startFrames.length; i++) {
                        if (startFrames[i].equals(lastFrames[i])) {
                            rtn += startFrames[i] + ",";
                        }else 
                            rtn += startFrames[i] +"-" +lastFrames[i] + ",";
                    }
                    if (rtn.endsWith(",")) rtn = rtn.substring(0, rtn.length()-1);
                }
            }
        }
        return rtn;
    }
    
    	
}
