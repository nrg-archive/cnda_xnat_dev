//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Created on Sep 8, 2005
 *
 */
package org.apache.turbine.app.cnda_xnat.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.utils.AdminUtils;

/**
 * @author Tim
 *
 */
public class Error  extends org.nrg.xdat.turbine.modules.screens.SecureScreen {

    /* (non-Javadoc)
     * @see org.apache.turbine.modules.screens.VelocitySecureScreen#doBuildTemplate(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
     */
    @Override
    protected void doBuildTemplate(RunData data, Context context) throws Exception {
        try {
            String s = data.getParameters().get("exception");
            if (s !=null)
                AdminUtils.sendErrorNotification(data, s, context);
        } catch (RuntimeException e) {
        }
    }

}
