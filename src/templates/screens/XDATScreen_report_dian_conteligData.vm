$page.setTitle("CONTELIG")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)
#if ($data.getParameters().getString("popup"))
#set ($popup = $data.getParameters().getString("popup") )
#set ($popup = "false")
#end
#if($project)

#else
#set($project=$om.getProject())
#end

<table width="100%">
	<tr>
		<td>
			#parse($turbineUtils.getTemplateName("_report",$om.getXSIType(),$project))
		</td>
		<td valign="top" align="right">
			#elementActionsBox($element $search_field $search_value $data.getSession().getAttribute("user") $item)
		</td>
	</tr>
	<tr>
		<td colspan="$column_count">

<style>
#uds_form {
	border-collapse: collapse;
	border-spacing:0px;
	border: 1px solid #000;
}
#uds_form td, #uds_form th {
	vertical-align:top;
	text-align:left;
	border:1px solid #000;
	min-width: 70px;
}
</style>


#set($column_count = $item.getChildItems("dian:conteligData/conteligList/contelig").size() + 1)
#set($elig_num = $item.getChildItems("dian:conteligData/conteligList/contelig").size() - 1)

<table id="uds_form">
	<tr>
		<th>Date of eligibility screening</th>
		#foreach($column_counter in [0..$elig_num])
            <td>$!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/DATE")</td>
        #end
	</tr>
	<tr>
	    <th colspan="$column_count">General Continuation Criteria<p>Both should be "No."  Contact Angie Berry at 314-286-2442 or berrya@abraxas.wustl.edu to request a deviation.</th>
	</tr>	
	
	<tr>
		<th style="padding-left:20px;">Participant has a medical or psychiatric illness that would interfere with completing assessments (e.g., pregnancy)</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGMED")
				#if ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGMED") == "1")
					- Yes
				#elseif ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGMED") == "0")
					- No
			    #end
            </td>
        #end
	</tr>
	<tr>
		<th style="padding-left:20px;">Participant resides in a skilled nursing facility or requires nursing home level care</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGNURS")
				#if ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGNURS") == "1")
					- Yes
				#elseif ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGNURS") == "0")
					- No
			    #end
            </td>
        #end
	</tr>
	<tr>
		<th colspan="$column_count">Procedure Specific Continuation Questions<p>If "Yes," conduct in-depth screening to ensure that the person can participat in the study procedure.</th>
	</tr>
	<tr>
		<th style="padding-left:20px;">MRI--Participant has metal implants or fragments, claustrophobia, or physical size that prohibits MRI</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGMRI")
				#if ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGMRI") == "1")
					- Yes
				#elseif ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGMRI") == "0")
					- No
			    #end
            </td>
        #end
	</tr>
	<tr>
		<th style="padding-left:20px;">PET--Participant has received within the last 12 months radiation above limits set by local institutional authorities</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGPET")
				#if ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGPET") == "1")
					- Yes
				#elseif ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGPET") == "0")
					- No
			    #end
            </td>
        #end
	</tr>
	<tr>
		<th style="padding-left:20px;">PET--Person is currently breastfeeding.  If "Yes" see protocol procedures manual for lactation guidelines.</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGPET2")
				#if ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGPET2") == "1")
					- Yes
				#elseif ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGPET2") == "0")
					- No
			    #end
            </td>
        #end
	</tr>	
	<tr>
		<th style="padding-left:20px;">LP--Participant is on blood thinners, has a coagulation disorder, or an active infectious process</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGLP")
				#if ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGLP") == "1")
					- Yes
				#elseif ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGLP") == "0")
					- No
			    #end
            </td>
        #end
	</tr>
	<tr>
		<th colspan="$column_count">Confounding Variables</th>
	</tr>	
	<tr>
		<th style="padding-left:20px;">Participant is aware of mutation status (status should not be disclosed to site)</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGMUT")
				#if ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGMUT") == "1")
					- Yes
				#elseif ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGMUT") == "0")
					- No
			    #end
            </td>
        #end
	</tr>
	<tr>
		<th style="padding-left:20px;">Participant has used an investigational drug for AD within 30 days of the screening visit or currently receives neuropsychiatric measures more than once a year (these may interfere with DIAN measures)</th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGNEUR")
				#if ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGNEUR") == "1")
					- Yes
				#elseif ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/ELIGNEUR") == "0")
					- No
			    #end
            </td>
        #end
	</tr>
	<tr>
		<th style="padding-left:20px;">Participant agrees (in principle) to complete all DIAN study procedures </th>
		#foreach($column_counter in [0..$elig_num])
            <td>
                $!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/PTAGREE")
				#if ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/PTAGREE") == "1")
					- Yes
				#elseif ($!item.getStringProperty("dian:conteligData/conteligList/contelig[$column_counter]/PTAGREE") == "0")
					- No
			    #end
            </td>
        #end
	</tr>
</table>
		</td>
	</tr>
</table>
