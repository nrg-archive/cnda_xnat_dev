<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">
#macro( propValueRow $name $prop )
  #if ( $!item.getProperty("$prop") )
		<tr><td class="attr_name">$name:</td><td class="attr_value">$item.getProperty("$prop")</td></tr>
	#end
#end

<!-- TODO: better presentation -->
#macro( propBoolValueRow $name $prop )
	#if ($!item.getBooleanProperty("$prop"))
		<tr><td class="attr_name">$name:</td><td class="attr_value">$item.getBooleanProperty("$prop")</td></tr>
	#end
#end

#macro(tableValueRow $name $field)
<tr>
  <td class="table_attr_name">$name</td>
	<td class="attr_value">$!item.getProperty($field)</td>
</tr>
#end

<style type="text/css">
td.attr_name { <!-- in imitation of xdat.css (th) -->
	border-collapse: collapse;
	border-left-style: none;
	border-right-style: none;
	border-top-style: none;
	font-size: 11px;
	font-weight: 700;
	line-height: 13px;
	margin: 0px;
	padding-left: 4px;
	padding-right: 4px;
}

td.attr_value { <!-- in imitation of xdat.css (td) -->
	font-family: verdana,geneva,helvetica;
	font-size: 10px;
	line-height: 14px;
	padding-left: 4px;
	padding-right: 4px;
}

td.subtable {
	valign: top;
}

</style>

$page.setTitle("Diagnoses : $!item.getProperty('cnda:diagnosesData.ID')")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)
<br>

#if ($data.message)
<div class="error">$data.message</div><br>
#end

#if ($data.getParameters().getString("popup"))
	#set ($popup = $data.getParameters().getString("popup") )
	#set ($popup = "false")
#end

#if (!$project)
#set($project=$item.getProperty("project"))
#end

#set($subject = $om.getSubjectData())
#set($part_id = $subject.getStringProperty("ID"))

<div class="edit_title">
Diagnoses:
#if($project)
	$!om.getIdentifier($project)
#else
	$!item.getProperty("cnda:diagnosesData.ID")
#end
</div>

#parse("/screens/workflow_alert.vm")

<table width="100%">  <!-- FIXME: ugh, nested tables. -->
	<tr>
		<td valign="top"><table>
			#propValueRow( "Date" "cnda:diagnosesData/date" )
			#propValueRow( "Time" "cnda:diagnosesData/time" )
			#propValueRow( "Visit ID" "cnda:diagnosesData/visit_id" )
			#propValueRow( "Diagnoser" "cnda:diagnosesData/diagnoser" )
		</table></td>
		<td valign="top"><table>
			<tr>
				<td class="attr_name">Subject:</td>
				<td>
					<A CLASS=b HREF="$link.setAction('DisplayItemAction').addPathInfo('search_element','xnat:subjectData').addPathInfo('search_field','xnat:subjectData.ID').addPathInfo('search_value',$part_id).addPathInfo('popup','$!popup').addPathInfo('project','$!project')">
					#if($project)
						$!subject.getIdentifier($project)
					#else
						$!subject.getId()
					#end
					</A>
				</td>
			</tr>
			#propValueRow( "Age" "cnda:diagnosesData/age" )
			#if($!subject.getGenderText())
			<tr>
				<td class="attr_name">Gender:</td>
				<td class="attr_value">$subject.getGenderText()</td>
			</tr>
			#end
			#propValueRow( "Handedness (Edinburgh)" "cnda:diagnosesData/handedness" )
			#if ($item.getBooleanProperty("cnda:diagnosesData/ineligibleFromSCID"))
				<tr><td><font style="bold" color="red">Ineligible from SCID</font></td></tr>
			#end
			#foreach($field in $subject.getAddid())
			<tr>
        <td class="attr_name">$field.getName():</td>
				<td class="attr_value">
					<A CLASS=b HREF="$link.setAction("DisplayItemAction").addPathInfo("search_element","xnat:subjectData").addPathInfo("search_field","xnat:subjectData.ID").addPathInfo("search_value",$part_id).addPathInfo("popup","$!popup").addPathInfo("project","$!project")">$field.getAddid()</A>
				</td>
			</tr>
			#end
		</table></td>
		<td valign="top">
#parse("/screens/xnat_experimentData_shareDisplay.vm")
		</td>
		<td valign="top">
#elementActionsBox($element $search_field $search_value $data.getSession().getAttribute("user") $item)
		</td>
	</tr>
</table>

#if ($!item.getStringProperty("note"))
<table>
  <tr>
		<td valign="top" class="attr_name">Notes:</td>
		<td valign="top" class="attr_value">$!item.getStringProperty("note")</td>
	</tr>
</table>
#end

<br>

<table>
	<tr><th>Tics Diagnosis</th></tr>
	#propValueRow("DSM-IV" "cnda:diagnosesData/ticDiagnosis/DSMIV")
	#propBoolValueRow("Impairment or Distress?" "cnda:diagnosesData/ticDiagnosis/impairDistress")
	#propValueRow("TSSG" "cnda:diagnosesData/ticDiagnosis/TSSG")
	#propValueRow("TSSG2" "cnda:diagnosesData/ticDiagnosis/TSSG2")
	#tableValueRow("Age at first diagnosis" "cnda:diagnosesData/firstDiagnosedTicsAge")

	#if ($!item.getDoubleProperty("cnda:diagnosesData/motor/firstTicAge") || $!item.getDoubleProperty("cnda:diagnosesData/vocal/firstTicAge") || $!item.getDoubleProperty("cnda:diagnosesData/motor/worstTicAge") || $!item.getDoubleProperty("cnda:diagnosesData/vocal/worstTicAge"))
	<tr><th></th><th>Motor</th><th>Vocal</th></tr>
	<tr>
		<td>Age at first tic</td>
		<td>$!item.getDoubleProperty("cnda:diagnosesData/motor/firstTicAge")</td>
		<td>$!item.getDoubleProperty("cnda:diagnosesData/vocal/firstTicAge")</td>
	</tr>
	<tr>
		<td>Age at worst tic</td>
		<td>$!item.getDoubleProperty("cnda:diagnosesData/motor/worstTicAge")</td>
		<td>$!item.getDoubleProperty("cnda:diagnosesData/vocal/worstTicAge")</td>
	</tr>
	#end

	<tr><th>Other Diagnoses</th></tr>
	#tableValueRow("ADHD (DSM-IV)" "cnda:diagnosesData/diagnosis_ADHD_DSMIV")
	#tableValueRow("TS DCI score" "cnda:diagnosesData/score_TS_DCI")
	#tableValueRow("OCD (DSM-IV)" "cnda:diagnosesData/diagnosis_OCD_DSMIV")	
	#tableValueRow("Age at first obsession" "cnda:diagnosesData/firstObsessionAge")
	#tableValueRow("Age at first compulsion" "cnda:diagnosesData/firstCompulsionAge")
	#tableValueRow("Age at worst obsessive/compulsive symptom" "cnda:diagnosesData/worstObsessiveCompulsiveSymptomAge")
</table



<!-- BEGIN cnda:diagnosesData/validation -->
#if ($!item.getProperty("cnda:diagnosesData/validation/date") || $!item.getStringProperty("cnda:diagnosesData/validation/method"))
<BR>
<font face="$ui.sansSerifFonts" size="2">Validation</font>
<TABLE>
	<TR><TD>Method:</TD><TD>$!item.getStringProperty("cnda:diagnosesData/validation/method")</TD></TR>
	<TR><TD>Date:</TD><TD>$!item.getProperty("cnda:diagnosesData/validation/date")</TD></TR>
	<TR><TD>Status:</TD><TD>$!item.getStringProperty("cnda:diagnosesData/validation/status")</TD></TR>
	<TR><TD>Notes:</TD><TD>$!item.getStringProperty("cnda:diagnosesData/validation/notes")</TD></TR>
</TABLE>
#end
<!-- END cnda:diagnosesData/validation -->


#set($xnat_experimentData_field_4_NUM_ROWS=$item.getChildItems("cnda:diagnosesData/fields/field").size() - 1)
#if($xnat_experimentData_field_4_NUM_ROWS>=0)
  #foreach($xnat_experimentData_field_4_COUNTER in [0..$xnat_experimentData_field_4_NUM_ROWS])
<!-- BEGIN cnda:diagnosesData/fields/field[$xnat_experimentData_field_4_COUNTER] -->
	<TABLE>
		<TR><TH align="left"><BR><font face="$ui.sansSerifFonts" size="2">cnda:diagnosesData/fields/field[$xnat_experimentData_field_4_COUNTER]</font></TH></TR>
		<TR>
			<TD align="left" valign="top">
				<TABLE>
					<TR><TD>field</TD><TD>$!item.getStringProperty("cnda:diagnosesData/fields/field[$xnat_experimentData_field_4_COUNTER]/field")</TD></TR>
					<TR><TD>name</TD><TD>$!item.getStringProperty("cnda:diagnosesData/fields/field[$xnat_experimentData_field_4_COUNTER]/name")</TD></TR>
				</TABLE>
			</TD>
		</TR>
	</TABLE>
<!-- END cnda:diagnosesData/fields/field[$xnat_experimentData_field_4_COUNTER] -->
	#end
#end

<!-- removed project info -->

<BR>
#parse("/screens/ReportProjectSpecificFields.vm")
